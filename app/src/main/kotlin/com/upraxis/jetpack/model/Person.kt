package com.upraxis.jetpack.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.upraxis.jetpack.utility.PERSON_TABLE_NAME

@Entity(tableName = PERSON_TABLE_NAME)
data class Person(@PrimaryKey val _id: Long,
                  @SerializedName("first_name") val firstName: String,
                  @SerializedName("last_name") val lastName: String)