package com.upraxis.jetpack.screen

import com.upraxis.jetpack.screen.person.details.PersonDetailsViewModel
import com.upraxis.jetpack.screen.person.list.PersonListViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

val viewModelModule: Module = module {
    viewModel { PersonListViewModel(get()) }
    viewModel { PersonDetailsViewModel(get()) }
}