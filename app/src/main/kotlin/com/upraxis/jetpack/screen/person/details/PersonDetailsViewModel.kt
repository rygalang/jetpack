package com.upraxis.jetpack.screen.person.details

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.upraxis.jetpack.base.BaseViewModel
import com.upraxis.jetpack.model.Person
import com.upraxis.jetpack.usecase.PersonLoader

class PersonDetailsViewModel(private val personLoader: PersonLoader) : BaseViewModel() {

    var personObservable: LiveData<Person> = MutableLiveData<Person>()

    fun load(personId: Long) {
        personObservable = personLoader.load(personId)
    }
}