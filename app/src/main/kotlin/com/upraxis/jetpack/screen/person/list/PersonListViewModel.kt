package com.upraxis.jetpack.screen.person.list

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableBoolean
import com.upraxis.jetpack.utility.rx.scheduler
import com.upraxis.jetpack.base.BaseViewModel
import com.upraxis.jetpack.model.Person
import com.upraxis.jetpack.usecase.PersonLoader
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

class PersonListViewModel(private val personLoader: PersonLoader) : BaseViewModel() {

    val personListObservable: MutableLiveData<List<Person>> = MutableLiveData()

    // We will bind this field in layout
    val isLoading: ObservableBoolean = ObservableBoolean()

    fun load() {
        isLoading.set(true)
        personLoader.load()
                .scheduler()
                .subscribe(object : SingleObserver<List<Person>> {
                    override fun onSuccess(list: List<Person>) {
                        isLoading.set(false)
                        personListObservable.postValue(list)
                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable.add(d)
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        isLoading.set(false)
                    }
                })
    }
}
