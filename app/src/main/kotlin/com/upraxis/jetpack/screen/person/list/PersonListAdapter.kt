package com.upraxis.jetpack.screen.person.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater.from
import android.view.ViewGroup
import com.upraxis.jetpack.databinding.ItemPersonBinding.inflate
import com.upraxis.jetpack.model.Person
import java.util.*

class PersonListAdapter(private val onClick: (person: Person) -> Unit)
    : RecyclerView.Adapter<PersonListViewHolder>() {

    private var items: List<Person> = ArrayList()

    fun set(items: List<Person>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonListViewHolder {
        val binder = inflate(from(parent.context), parent, false)
        return PersonListViewHolder(binder)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: PersonListViewHolder, position: Int) {
        val person = items[position]
        holder.bind(person)
        holder.itemView.setOnClickListener {
            onClick.invoke(person)
        }
    }
}