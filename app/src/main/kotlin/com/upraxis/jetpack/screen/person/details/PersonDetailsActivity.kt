package com.upraxis.jetpack.screen.person.details

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.upraxis.jetpack.R
import com.upraxis.jetpack.databinding.ActivityPersonDetailsBinding
import com.upraxis.jetpack.model.Person
import org.koin.android.viewmodel.ext.android.getViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class PersonDetailsActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_ID = "id"

        fun intent(context: Context, id: Long): Intent {
            Timber.d("$id")
            return Intent(context, PersonDetailsActivity::class.java)
                    .putExtra(EXTRA_ID, id)
        }
    }

    private val personDetailsViewModel: PersonDetailsViewModel by viewModel()
    private lateinit var binding: ActivityPersonDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_person_details)
        
        personDetailsViewModel.load(id())
    }

    override fun onStart() {
        super.onStart()
        personDetailsViewModel.personObservable.observe(this, Observer { show(it) })
    }

    private fun show(person: Person?) {
        supportActionBar.apply {
            title = person?.firstName.plus(" ").plus(person?.lastName)
        }
    }

    private fun id() = intent.getLongExtra(EXTRA_ID, 0)
}