package com.upraxis.jetpack.screen.person.list

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.widget.Toast
import com.upraxis.jetpack.R
import com.upraxis.jetpack.databinding.ActivityPersonListBinding
import com.upraxis.jetpack.model.Person
import com.upraxis.jetpack.screen.person.details.PersonDetailsActivity
import kotlinx.android.synthetic.main.activity_person_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class PersonListActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {

    //Request a PersonListViewModel instance.
    private val personListViewModel by viewModel<PersonListViewModel>()

    private lateinit var adapter: PersonListAdapter

    private lateinit var binding: ActivityPersonListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_person_list)
        binding.viewModel = personListViewModel

        personListViewModel.load()
    }

    override fun onStart() {
        super.onStart()
        setUpRefreshPersonList()
        setUpPersonList()

        //Observe changes from MutableLiveData.
        personListViewModel.personListObservable.observe(this,
                Observer { list -> showList(list.orEmpty()) })
    }

    override fun onRefresh() {
        personListViewModel.load()
    }

    private fun setUpRefreshPersonList() {
        refreshPersonList.setOnRefreshListener(this)
    }

    private fun setUpPersonList() {
        adapter = PersonListAdapter(this::click) // or
//        PersonListAdapter {
//            Toast.makeText(this,
//                    "$it",
//                    Toast.LENGTH_SHORT).show()
//        }

        personList.adapter = adapter
        personList.addItemDecoration(DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL))
    }

    private fun showList(list: List<Person>) {
        adapter.set(list)
    }

    private fun click(person: Person) {
        startActivity(PersonDetailsActivity.intent(this, person._id))
    }
}