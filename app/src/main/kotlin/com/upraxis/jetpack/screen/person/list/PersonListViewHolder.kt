package com.upraxis.jetpack.screen.person.list

import android.support.v7.widget.RecyclerView
import com.upraxis.jetpack.databinding.ItemPersonBinding
import com.upraxis.jetpack.model.Person

class PersonListViewHolder(private val binding: ItemPersonBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(person: Person) {
        binding.person = person
        binding.executePendingBindings()
    }
}