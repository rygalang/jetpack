package com.upraxis.jetpack.base

import org.koin.log.Logger
import timber.log.Timber

/**
 * Logger that uses Timber.
 * */
class JetPackLogger : Logger {
    override fun debug(msg: String) {
        Timber.d(msg)
    }

    override fun err(msg: String) {
        Timber.e(msg)
    }

    override fun info(msg: String) {
        Timber.i(msg)
    }
}