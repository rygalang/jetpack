package com.upraxis.jetpack.base

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

abstract class BaseViewModel : ViewModel() {

    val disposable = CompositeDisposable()

    override fun onCleared() {
        Timber.d("onCleared")
        disposable.dispose()
        super.onCleared()
    }
}