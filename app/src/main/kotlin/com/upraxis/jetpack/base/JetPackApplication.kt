package com.upraxis.jetpack.base

import android.app.Application
import com.upraxis.jetpack.BuildConfig
import com.upraxis.jetpack.di.jetPackModules
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger
import org.koin.log.EmptyLogger
import timber.log.Timber

class JetPackApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            initTimber()
        }
        startKoin()
    }

    private fun initTimber() {
        Timber.plant(Timber.DebugTree())
    }

    private fun startKoin() {
        if (BuildConfig.DEBUG) {
            startKoin(this, jetPackModules, logger = JetPackLogger())
        } else {
            startKoin(this, jetPackModules, logger = EmptyLogger())
        }
    }
}