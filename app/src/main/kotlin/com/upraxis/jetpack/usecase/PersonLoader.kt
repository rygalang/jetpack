package com.upraxis.jetpack.usecase

import android.arch.lifecycle.LiveData
import com.upraxis.jetpack.model.Person
import io.reactivex.Single

interface PersonLoader {

    /**
     * @return a [Single] that emit a [List] of [Person].
     * */
    fun load(): Single<List<Person>>

    /**
     * @param personId id of person
     * @return a [LiveData] that emit a [Person]
     * */
    fun load(personId: Long): LiveData<Person>
}