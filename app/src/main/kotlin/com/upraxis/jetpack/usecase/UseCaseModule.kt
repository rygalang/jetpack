package com.upraxis.jetpack.usecase

import org.koin.dsl.module.module

val useCaseModule = module {

    factory<PersonLocalLoader> { DefaultPersonLocalLoader(get()) }
    factory<PersonLocalSaver> { DefaultPersonLocalSaver(get()) }

    factory<PersonRemoteLoader> { DefaultPersonRemoteLoader(get()) }

    factory<PersonLoader> { DefaultPersonLoader(get(), get(), get()) }
}