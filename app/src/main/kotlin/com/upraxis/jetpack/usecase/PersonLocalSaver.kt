package com.upraxis.jetpack.usecase

import com.upraxis.jetpack.model.Person
import io.reactivex.Completable

interface PersonLocalSaver {

    /**
     * @return a [Completable] that emit an error or completion on saving [List] of [Person].
     * */
    fun save(list: List<Person>): Completable
}