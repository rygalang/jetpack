package com.upraxis.jetpack.usecase

import com.upraxis.jetpack.utility.retrofit.Persons
import com.upraxis.jetpack.model.Person
import com.upraxis.jetpack.utility.retrofit.ApiFactory
import io.reactivex.Single

class DefaultPersonRemoteLoader(private val apiFactory: ApiFactory) : PersonRemoteLoader {

    override fun all(): Single<List<Person>> = apiFactory.create(Persons::class.java)
            .flatMap { persons -> persons.loadPersons() }
}