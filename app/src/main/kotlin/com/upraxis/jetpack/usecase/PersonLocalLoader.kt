package com.upraxis.jetpack.usecase

import android.arch.lifecycle.LiveData
import com.upraxis.jetpack.model.Person
import io.reactivex.Single

interface PersonLocalLoader {

    /**
     * @return a [Single] that emit a [List] of [Person] from database.
     * */
    fun all(): Single<List<Person>>

    /**
     * @return a [LiveData] that emit a [Person].
     * @param id the id of a [Person].
     * */
    fun byId(id: Long): LiveData<Person>
}