package com.upraxis.jetpack.usecase

import com.upraxis.jetpack.model.Person
import com.upraxis.jetpack.utility.room.PersonDao
import io.reactivex.Completable

internal class DefaultPersonLocalSaver(private val personDao: PersonDao) : PersonLocalSaver {


    override fun save(list: List<Person>): Completable {
        return Completable.defer {
            personDao.insert(list)
            Completable.complete()
        }
    }
}