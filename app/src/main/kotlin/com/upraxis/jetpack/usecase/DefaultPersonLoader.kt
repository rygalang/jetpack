package com.upraxis.jetpack.usecase

import com.upraxis.jetpack.model.Person
import io.reactivex.Single

class DefaultPersonLoader(private val local: PersonLocalLoader,
                          private val remote: PersonRemoteLoader,
                          private val saver: PersonLocalSaver) : PersonLoader {

    override fun load(): Single<List<Person>> {
        return remote.all()
                .flatMapCompletable { saver.save(it) }
                .andThen(local.all())
                .onErrorResumeNext(local.all())
    }

    override fun load(personId: Long)= local.byId(personId)
}