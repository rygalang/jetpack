package com.upraxis.jetpack.usecase

import com.upraxis.jetpack.model.Person
import io.reactivex.Single

interface PersonRemoteLoader {

    /**
     * @return a [Single] that emit a [List] of [Person] from remote.
     * */
    fun all(): Single<List<Person>>
}