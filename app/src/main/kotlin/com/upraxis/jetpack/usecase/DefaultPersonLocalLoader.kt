package com.upraxis.jetpack.usecase

import com.upraxis.jetpack.utility.room.PersonDao

class DefaultPersonLocalLoader(private val personDao: PersonDao) : PersonLocalLoader {

    override fun all() = personDao.all()

    override fun byId(id: Long) = personDao.byId(id)
}