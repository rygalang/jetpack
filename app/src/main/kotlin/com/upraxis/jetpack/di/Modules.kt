package com.upraxis.jetpack.di

import com.upraxis.jetpack.screen.viewModelModule
import com.upraxis.jetpack.usecase.useCaseModule
import com.upraxis.jetpack.utility.utilityModule


val jetPackModules = listOf(utilityModule, viewModelModule, useCaseModule)