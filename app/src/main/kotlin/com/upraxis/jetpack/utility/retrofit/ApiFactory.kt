package com.upraxis.jetpack.utility.retrofit

import io.reactivex.Single

interface ApiFactory {

    fun <T> create(apiClass: Class<T>): Single<T>
}
