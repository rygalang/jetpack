package com.upraxis.jetpack.utility.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.upraxis.jetpack.model.Person

@Database(
        entities = [Person::class],
        version = 1
)
abstract class JetPackDatabase : RoomDatabase() {

    abstract fun personDao(): PersonDao
}