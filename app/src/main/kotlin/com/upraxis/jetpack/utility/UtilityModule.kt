package com.upraxis.jetpack.utility

import android.arch.persistence.room.Room
import com.upraxis.jetpack.utility.retrofit.ApiFactory
import com.upraxis.jetpack.utility.retrofit.DefaultApiFactory
import com.upraxis.jetpack.utility.room.JetPackDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

val utilityModule = module {
    //Single instance of DefaultApiFactory
    single<ApiFactory> { DefaultApiFactory() }

    //Single instance of RoomDatabase
    single {
        Room.databaseBuilder(androidContext(), JetPackDatabase::class.java, "jetpack-database.sql")
                .build()
    }

    factory { (get() as JetPackDatabase).personDao() }
}