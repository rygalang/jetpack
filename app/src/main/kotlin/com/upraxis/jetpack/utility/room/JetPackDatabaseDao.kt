package com.upraxis.jetpack.utility.room

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.upraxis.jetpack.model.Person
import com.upraxis.jetpack.utility.PERSON_TABLE_NAME
import io.reactivex.Single

@Dao
interface PersonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: List<Person>)

    @Query("SELECT * FROM $PERSON_TABLE_NAME")
    fun all(): Single<List<Person>>

    @Query("SELECT * FROM $PERSON_TABLE_NAME WHERE \"_id\" = :id")
    fun byId(id: Long): LiveData<Person>

}