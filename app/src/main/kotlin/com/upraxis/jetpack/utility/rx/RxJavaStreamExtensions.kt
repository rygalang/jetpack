package com.upraxis.jetpack.utility.rx

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * RxJava stream extension functions that subscribes to separate thread and observe on main thread.
 * */

fun <T> Flowable<T>.scheduler(): Flowable<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
}

fun <T> Observable<T>.scheduler(): Observable<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
}

fun <T> Single<T>.scheduler(): Single<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
}

fun Completable.scheduler(): Completable {
    return this.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
}

fun <T> Maybe<T>.scheduler(): Maybe<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
}



