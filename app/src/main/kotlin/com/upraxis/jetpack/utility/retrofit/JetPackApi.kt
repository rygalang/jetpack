package com.upraxis.jetpack.utility.retrofit

import com.upraxis.jetpack.model.Person
import io.reactivex.Single
import retrofit2.http.GET

interface Persons {

    @GET("person")
    fun loadPersons(): Single<List<Person>>
}