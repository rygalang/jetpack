package com.upraxis.jetpack.utility.retrofit

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

internal class DefaultApiFactory : ApiFactory {

    override fun <T> create(apiClass: Class<T>) = retrofit().map { t -> t.create(apiClass) }

    private val client: () -> Single<OkHttpClient> = {
        val builder = OkHttpClient.Builder()
        builder.addInterceptor(loggingInterceptor())
        Single.fromCallable { builder.build() }
    }

    private val loggingInterceptor: () -> HttpLoggingInterceptor = {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        interceptor
    }

    private val retrofit: () -> Single<Retrofit> = {
        client().flatMap { client -> intoRetrofit(client) }
    }

    private fun intoRetrofit(client: OkHttpClient): Single<Retrofit> {
        return Single.fromCallable {
            Retrofit.Builder()
                    .baseUrl("https://raw.githubusercontent.com/rygalang/android-exam/develop/rest_api/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }
    }
}