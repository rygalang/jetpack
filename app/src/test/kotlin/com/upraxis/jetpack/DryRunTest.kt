package com.upraxis.jetpack

import android.app.Application
import com.upraxis.jetpack.di.jetPackModules
import org.junit.Test
import org.koin.android.ext.koin.with
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.test.KoinTest
import org.koin.test.dryRun
import org.mockito.Mockito.mock

/**
 * DryRun will try to make each definition or module.
 */
class DryRunTest : KoinTest {

    @Test
    fun jetPackModuleDryRun() {
        startKoin(jetPackModules) with (mock(Application::class.java))
        dryRun()
    }
}