package com.upraxis.jetpack

import android.arch.persistence.room.Room
import com.upraxis.jetpack.utility.room.JetPackDatabase
import org.koin.dsl.module.module

val roomTestModule = module {
    single {
        // In-Memory database config
        Room.inMemoryDatabaseBuilder(get(), JetPackDatabase::class.java)
                .allowMainThreadQueries()
                .build()
    }
    factory { (get() as JetPackDatabase).personDao() }
}