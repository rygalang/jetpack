package com.upraxis.jetpack.usecase

import android.app.Application
import com.upraxis.jetpack.model.Person
import com.upraxis.jetpack.roomTestModule
import com.upraxis.jetpack.utility.room.PersonDao
import io.reactivex.observers.TestObserver
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.android.ext.koin.with
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.inject
import org.koin.test.AutoCloseKoinTest
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.util.*

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class DefaultPersonLocalLoaderTest : AutoCloseKoinTest() {

    private val personDao: PersonDao by inject()

    private lateinit var classUnderTest: DefaultPersonLocalLoader

    @Before
    fun setUp() {
        startKoin(listOf(roomTestModule)) with (mock(Application::class.java))

        classUnderTest = DefaultPersonLocalLoader(personDao)
    }

    @Test
    fun `should return list of person`() {

        val expected = mutableListOf(person(), person(), person())

        personDao.insert(expected)

        val testsObserver: TestObserver<List<Person>> = classUnderTest.all().test()

        testsObserver.assertComplete()
        testsObserver.assertValueCount(1)

        val actual = testsObserver.values().firstOrNull()

        assertThat(actual).isNotEmpty()
        assertThat(actual).containsAll(expected)
    }

    private fun person() = Person((Long.MIN_VALUE until Long.MAX_VALUE).random(), UUID.randomUUID()
            .toString(),
            UUID
            .randomUUID()
            .toString())

    fun ClosedRange<Long>.random() =
            Random().nextLong() +  start
}